// ============================================================================
//
// CS 411 - Assignment 5
// 
// ============================================================================

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "GL/glut.h"

typedef struct {
	float x, y, z;
} Vec3;

typedef struct{
	float x,y,z;
	Vec3 normal;
} Vertex;

typedef struct{
	int v0,v1,v2;
} Face;


typedef struct{
	int    vNum;          // number of vertices
	int    fNum;          // number of faces
	Vertex vLst[100000];  // vertex list
	Face   fLst[100000];  // face list
} Obj;


Obj o1;
Vec3 center;
float xmin = 0.0, xmax = 0.0,
	ymin = 0.0, ymax = 0.0,
	zmin = 0.0, zmax = 0.0,
	ah = 0.0, av = 0.0,
	lx = 0.0, ly = 0.0, lz = 3.0;

// ====================================
// Inputfile Reader and Processing
// ====================================

int readObjFile(char *fn, Obj &o)
{
	char c;
	FILE* fin;
	float x,y,z;
	int i, minIndx;

	// open input file
	if(! (fin=fopen(fn,"r")) ){
		printf("\nCan't open file %s\n\7",fn);
		return 0;
	}

	// read objects
	o.vNum=o.fNum=0;
	while(fscanf(fin,"%c",&c)!=EOF) {

		// remove white space
		while(c== ' ' || c =='\n'){
			if(fscanf(fin,"%c",&c)==EOF) return(1);
		}

		switch(c){
			// add vertex
		case 'v':
			fscanf(fin,"%f%f%f",&x,&y,&z);
			o.vLst[o.vNum].x=x;
			o.vLst[o.vNum].y=y;
			o.vLst[o.vNum].z=z;
			o.vNum++;
			break;

		case 'f':
			// add face
			fscanf(fin,"%d%d%d",&(o.fLst[o.fNum].v0),&(o.fLst[o.fNum].v1),
				&(o.fLst[o.fNum].v2));
			o.fNum++;
			break;
		}

		// remove trailing characters to end of line
		while(fread(&c,1,1,fin)) if (c== '\n') break;
	}

	// find the minimal vertex index
	minIndx=o.fLst[0].v0;
	for (i=0; i<o.fNum; i++) {
		if (o.fLst[i].v0<minIndx) minIndx=o.fLst[i].v0;
		if (o.fLst[i].v1<minIndx) minIndx=o.fLst[i].v1;
		if (o.fLst[i].v2<minIndx) minIndx=o.fLst[i].v2;
	}

	// normalize vertex indexes to start with 0
	if(minIndx) for (i=0; i<o.fNum; i++) {
		o.fLst[i].v0-=minIndx; 
		o.fLst[i].v1-=minIndx; 
		o.fLst[i].v2-=minIndx;
	}

	// return success
	fclose(fin);
	return(1);
}

void dumpObj(Obj &o)
{
	int i;

	printf("\n[INFO] Loading Vertices and Faces...");
	printf("Number of vertices: %d\n", o.vNum);
	printf("Number of faces:    %d\n\n", o.fNum);

	// dump the first three vertices
	for(i=0;i<3 && i<o.vNum;i++)
		printf("v[%d]=(%f,%f,%f)\n",i,o.vLst[i].x,o.vLst[i].y,o.vLst[i].z);

	// dump the first three faces
	for(i=0;i<3 && i<o.fNum;i++)
		printf("f[%d]=(%d,%d,%d)\n",i,o.fLst[i].v0,o.fLst[i].v1,o.fLst[i].v2);

	printf("\n");
}

// ====================================
// Vector calculation.
// ====================================

float dot(const Vec3 &a, const Vec3 &b) {
	return(a.x * b.x + a.y * b.y + a.z * b.z);
}

float len(const Vec3 &v) {
	return sqrt(dot(v, v));
}

Vec3 norm(const Vec3 &v) {
	Vec3 result;
	result.x = v.x / len(v);
	result.y = v.y / len(v);
	result.z = v.z / len(v);
	return result;
}

Vec3 cross(const Vec3 &a, const Vec3 &b) {
	Vec3 result;
	result.x = a.y * b.z - a.z * b.y;
	result.y = a.z * b.x - a.x * b.z;
	result.z = a.x * b.y - a.y * b.x;

	return result;
}

// Compute the normal vector for each vector.
// The result will store in Obj::vLst[*]::n*
void computeNormal() {
	for (int ii = 0; ii < o1.fNum; ii ++) {
		Vertex a, b, c;

		// Calculate normal vector.
		a.normal.x = o1.vLst[o1.fLst[ii].v1].x - o1.vLst[o1.fLst[ii].v0].x;
		a.normal.y = o1.vLst[o1.fLst[ii].v1].y - o1.vLst[o1.fLst[ii].v0].y;
		a.normal.z = o1.vLst[o1.fLst[ii].v1].z - o1.vLst[o1.fLst[ii].v0].z;

		b.normal.x = o1.vLst[o1.fLst[ii].v2].x - o1.vLst[o1.fLst[ii].v0].x;
		b.normal.y = o1.vLst[o1.fLst[ii].v2].y - o1.vLst[o1.fLst[ii].v0].y;
		b.normal.z = o1.vLst[o1.fLst[ii].v2].z - o1.vLst[o1.fLst[ii].v0].z;

		c.normal = cross(a.normal, b.normal);
		c.normal = norm(c.normal);

		// Add vector into vectice lists.
		o1.vLst[o1.fLst[ii].v0].normal.x += c.normal.x;
		o1.vLst[o1.fLst[ii].v0].normal.y += c.normal.y;
		o1.vLst[o1.fLst[ii].v0].normal.z += c.normal.z;

		o1.vLst[o1.fLst[ii].v1].normal.x += c.normal.x;
		o1.vLst[o1.fLst[ii].v1].normal.y += c.normal.y;
		o1.vLst[o1.fLst[ii].v1].normal.z += c.normal.z;

		o1.vLst[o1.fLst[ii].v2].normal.x += c.normal.x;
		o1.vLst[o1.fLst[ii].v2].normal.y += c.normal.y;
		o1.vLst[o1.fLst[ii].v2].normal.z += c.normal.z;
	}

	// Normalize all vertice normal.
	for (int ii = 0; ii < o1.fNum; ii ++) {

		o1.vLst[o1.fLst[ii].v0].normal = norm(o1.vLst[o1.fLst[ii].v0].normal);
		o1.vLst[o1.fLst[ii].v1].normal = norm(o1.vLst[o1.fLst[ii].v1].normal);
		o1.vLst[o1.fLst[ii].v2].normal = norm(o1.vLst[o1.fLst[ii].v2].normal);

	}
}

void computeCenter(void)
{
	for(int ii=0; ii<o1.vNum; ii++)
	{
		center.x += o1.vLst[ii].x;
		center.y += o1.vLst[ii].y;
		center.z += o1.vLst[ii].z;
	}

	center.x /= o1.vNum;
	center.y /= o1.vNum;
	center.z /= o1.vNum;
}

void normalizeVertex(void)
{
	xmin = xmax = o1.vLst[0].x;
	ymin = ymax = o1.vLst[0].y;
	zmin = zmax = o1.vLst[0].z;

	for(int ii=0; ii<o1.vNum; ii++)
	{
		if(o1.vLst[ii].x<xmin)
			xmin = o1.vLst[ii].x;
		else if(o1.vLst[ii].x > xmax)
			xmax = o1.vLst[ii].x;

		if(o1.vLst[ii].y < ymin)
			ymin = o1.vLst[ii].y;
		else if(o1.vLst[ii].y > ymax)
			ymax = o1.vLst[ii].y;

		if(o1.vLst[ii].z < zmin)
			zmin = o1.vLst[ii].z;
		else if(o1.vLst[ii].z > zmax)
			zmax = o1.vLst[ii].z;
	}

	float factor = xmax - xmin;
	if((ymax - ymin) > factor)
		factor = ymax - ymin;
	if((zmax - zmin) > factor)
		factor = zmax - zmin;

	for(int ii = 0; ii < o1.vNum; ii++)
	{
		o1.vLst[ii].x = o1.vLst[ii].x / factor;
		o1.vLst[ii].y = o1.vLst[ii].y / factor;
		o1.vLst[ii].z = o1.vLst[ii].z / factor;
	}
}

// ======================================
// OpenGL Functions
// ======================================
void init() {
	glClearColor(0.0, 0.0, 0.0, 0.0);
	// glShadeModel(GL_SMOOTH);

	computeNormal();
	normalizeVertex();
	computeCenter();

	// Light
	float light_position[] = {0.0, 0.0, 50.0, 1.0};
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);

	float light_ambient[] = {0.0, 0.0, 0.0, 0.0};
	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);

	float light_diffuse[] = {1.0, 1.0, 1.0, 1.0};
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);

	float light_specular[] = {1.0, 1.0, 1.0, 1.0};
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);


	// Material
	float mat_ambient[] = {0.0, 0.5, 1.0, 1.0};
	glMaterialfv(GL_FRONT, GL_AMBIENT, mat_ambient);

	float mat_diffuse[] = {0.5, 0.5, 0.5, 1.0};
	glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse);

	float mat_specular[] = {1.0, 1.0, 1.0, 1.0};
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);

	float mat_shininess[] = {128.0};
	glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_DEPTH_TEST);

	glMatrixMode(GL_PROJECTION);
	glFrustum(-40.0, 40.0, -60.0, 60.0, 25.0, 125.0);
}

void display() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	gluLookAt(lx, ly, lz, 0.0, 0.0, 0.0, 0.0, 2.0, 0.0);    

	glPushMatrix();
	glRotatef(av, 1, 0, 0);
	glRotatef(ah, 0, 1, 0);
	glTranslatef(-center.x, -center.y, -center.z);

	// Draw all faces
	for(int i = 0; i < o1.fNum; i ++) {
		// preprocess vertice info
		int p[3] = { o1.fLst[i].v0, o1.fLst[i].v1, o1.fLst[i].v2 };

		glPolygonMode(GL_FRONT,	GL_FILL);
		glPolygonMode(GL_BACK,	GL_LINE);

		glBegin(GL_TRIANGLES);
			glShadeModel(GL_FLAT);
			glNormal3f(o1.vLst[p[0]].normal.x,	o1.vLst[p[0]].normal.y,		o1.vLst[p[0]].normal.z);
			glVertex3f(o1.vLst[p[0]].x,			o1.vLst[p[0]].y,			o1.vLst[p[0]].z);
			glNormal3f(o1.vLst[p[1]].normal.x,	o1.vLst[p[1]].normal.y,		o1.vLst[p[1]].normal.z);
			glVertex3f(o1.vLst[p[1]].x,			o1.vLst[p[1]].y,			o1.vLst[p[1]].z);
			glNormal3f(o1.vLst[p[2]].normal.x,	o1.vLst[p[2]].normal.y,		o1.vLst[p[2]].normal.z);
			glVertex3f(o1.vLst[p[2]].x,			o1.vLst[p[2]].y,			o1.vLst[p[2]].z);
		glEnd();
	}

	glPopMatrix();
	glFlush();
}

void reshape(int w, int h) {
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(52.0, (GLfloat)w/h, 1, 100);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void keyboard(unsigned char key, int x, int y) {
	printf("\n[INFO] KEY PRESSED %c\n", key);

	switch (key)
	{
	case '-':
		lz += 0.2;
		glutPostRedisplay();
		break;
	case '=':
		lz -= 0.2;
		glutPostRedisplay();
		break;
	case 'i':
		Vec3 c;
		c.x = o1.vLst[17].x - center.x;
		c.y = o1.vLst[17].y - center.y;
		c.z = o1.vLst[17].z - center.z;

		if (dot(center, o1.vLst[17].normal) < 0) {
			for (int ii = 0; ii < o1.fNum; ii ++) {
				o1.vLst[o1.fLst[ii].v0].normal.x = -o1.vLst[o1.fLst[ii].v0].normal.x;
				o1.vLst[o1.fLst[ii].v0].normal.y = -o1.vLst[o1.fLst[ii].v0].normal.y;
				o1.vLst[o1.fLst[ii].v0].normal.z = -o1.vLst[o1.fLst[ii].v0].normal.z;
				o1.vLst[o1.fLst[ii].v1].normal.x = -o1.vLst[o1.fLst[ii].v1].normal.x;
				o1.vLst[o1.fLst[ii].v1].normal.y = -o1.vLst[o1.fLst[ii].v1].normal.y;
				o1.vLst[o1.fLst[ii].v1].normal.z = -o1.vLst[o1.fLst[ii].v1].normal.z;
				o1.vLst[o1.fLst[ii].v2].normal.x = -o1.vLst[o1.fLst[ii].v2].normal.x;
				o1.vLst[o1.fLst[ii].v2].normal.y = -o1.vLst[o1.fLst[ii].v2].normal.y;
				o1.vLst[o1.fLst[ii].v2].normal.z = -o1.vLst[o1.fLst[ii].v2].normal.z;
			}
		}
		break;
	default:
		break;
	}

	glutPostRedisplay();
}

void special(int key, int x, int y) {
	switch (key) {
	case GLUT_KEY_LEFT:
		ah += 10;
		glutPostRedisplay();
		break;
	case GLUT_KEY_RIGHT:
		ah -= 10;
		glutPostRedisplay();
		break;
	case GLUT_KEY_UP:
		av += 10;
		glutPostRedisplay();
		break;
	case GLUT_KEY_DOWN:
		av -= 10;
		glutPostRedisplay();
		break;
	}
	glutPostRedisplay();
}

void help() {
	printf("%s%s%s%s", 
		"\n[OUTPUT] USAGE\n",
		"key \"+\": Zoom out\n",
		"key \"-\": Zoom in\n",
		"arrow keys: move camera\n");
}


int main(int argc, char *argv[])
{

	// check transferred arguments
	if(argc<2){
		printf("\nUsage draw3d <objFile>\n\n");
		exit(0);
	}

	// load and dump the object file
	if(!readObjFile(argv[1], o1)){
		printf("Error: Can't read file %s\n",argv[1]);
	}

	printf("\n\n");
	printf("Object file name:   %s\n",argv[1]);
	dumpObj(o1);

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(640, 480);
	glutInitWindowPosition(100, 100);
	glutCreateWindow(argv[0]);
	printf("\n[INFO] OpenGL Version: %s\n", glGetString(GL_VERSION));

	init();
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutKeyboardFunc(keyboard);
	glutSpecialFunc(special);

	glutMainLoop();

	return 0;
}